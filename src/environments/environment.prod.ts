export const environment = {
  production: true,
  apiJavaUrl: 'http://localhost:8081',
  // apiNodeUrl: 'http://localhost:3000'
  apiNodeUrl: 'https://marthelo-nodebackcontract.herokuapp.com'
};
