import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Contract } from 'src/app/model/contract';
import { ContractsService } from 'src/app/service/contracts.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-contract',
  templateUrl: './new-contract.component.html',
  styleUrls: ['./new-contract.component.css'],
})
export class NewContractComponent implements OnInit {
  contractForm!: FormGroup;
  contractId?: string | null;
  contract!: Contract;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private newContractService: ContractsService
  ) {}

  ngOnInit(): void {
    this.contract = new Contract();
    this.contractForm = new FormGroup({      
      inceptionDate: new FormControl(''),
      companyName: new FormControl(''),
      emailAddress: new FormControl(''),
      coverageName: new FormControl(''),
      windSpeedThresholdCoverage: new FormControl(''),
      cityRiskName: new FormControl(''),
      // thirdPartyMeteoStation: new FormControl(''),
      windTurbineNumber: new FormControl(''),
      // daylyIndemnityPerWindTurbine: new FormControl(''),
      // maxIndemnity: new FormControl(''),
      // numberOfDayOfIndemnity: new FormControl(''),
      // premiumRate: new FormControl(''),
      // premium: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
    });

    this.contractId = this.route.snapshot.paramMap.get('id');
    console.log('savigny :' + this.contractId);
    if (this.contractId !== null) {
      console.log('if : ', this.contractId);

      this.newContractService
        .getContractByReference(this.contractId)
        .subscribe((data) => {
          console.log('data : ', data.data);
          this.contract = data.data;
          this.contract.contractId = data.data._id;
          this.contractForm.patchValue({
            inceptionDate: data.data.inceptionDate,
            companyName: data.data.companyName,
            emailAddress: data.data.emailAddress,
            coverageName: data.data.coverageName,
            windSpeedThresholdCoverage: data.data.windSpeedThresholdCoverage,
            cityRiskName: data.data.cityRiskName,
            // thirdPartyMeteoStation: data.data.thirdPartyMeteoStation,
            windTurbineNumber: data.data.windTurbineNumber,
            // daylyIndemnityPerWindTurbine: data.data.daylyIndemnityPerWindTurbine,
            // maxIndemnity: data.data.maxIndemnity,
            // numberOfDayOfIndemnity: data.data.numberOfDayOfIndemnity,
            // premiumRate: data.data.premiumRate,
            // premium: data.data.premium,
            startDate: data.data.startDate,
            endDate: data.data.endDate,            
          });
        });        
    }    
  }

  formValidation() {
console.log(this.contractForm.value);

    this.contract.inceptionDate = this.contractForm.value.inceptionDate;
    this.contract.companyName = this.contractForm.value.companyName;
    this.contract.emailAddress = this.contractForm.value.emailAddress;
    this.contract.coverageName = this.contractForm.value.coverageName;
    this.contract.windSpeedThresholdCoverage = this.contractForm.value.windSpeedThresholdCoverage;
    this.contract.cityRiskName = this.contractForm.value.cityRiskName;
    // this.contract.thirdPartyMeteoStation = this.contractForm.value.thirdPartyMeteoStation;
    this.contract.windTurbineNumber = this.contractForm.value.windTurbineNumber;
    // this.contract.daylyIndemnityPerWindTurbine = this.contractForm.value.daylyIndemnityPerWindTurbine;
    // this.contract.maxIndemnity = this.contractForm.value.maxIndemnity;
    // this.contract.numberOfDayOfIndemnity = this.contractForm.value.numberOfDayOfIndemnity;
    // this.contract.premiumRate = this.contractForm.value.premiumRate;
    // this.contract.premium = this.contractForm.value.premium;
    this.contract.startDate = this.contractForm.value.startDate;
    this.contract.endDate = this.contractForm.value.endDate;

    if (this.contract.contractReference) {
      this.newContractService.updateContract(this.contract).subscribe(data =>{
        console.log("data : ", data);
        
      })
    }else{      

    console.log('contract in component ', this.contract);
    this.newContractService
      .saveContract(this.contract)
      .subscribe((data: any) => {
        console.log(data);
        setTimeout(() => {
          this.router.navigate(['./admin/contracts']);
        }, 1000);
      });
  }
}


// delete(contractId:String){
// console.log("contractId : ", contractId);
// this.newContractService.delete(contractId).subscribe((data: any) =>{
//   console.log("data", data);
  
// })

// }

  demo() {
    this.contractForm.setValue({
      inceptionDate: "2021-12-02",
      companyName: 'VARRIN',
      emailAddress: 'hvarrin@gmail.com',
      coverageName: 'Insuffisance de vent',
      windSpeedThresholdCoverage: 15,
      cityRiskName: 'Fos-sur-mer',
      // thirdPartyMeteoStation: 'Visual Crossing',
      windTurbineNumber: 1,
      // daylyIndemnityPerWindTurbine: 150,
      // maxIndemnity: ,
      // numberOfDayOfIndemnity: 0,
      // premiumRate: 0,
      // premium: 0,
      startDate: "2021-01-01",
      endDate: "2021-06-30",
    });
  }



}
