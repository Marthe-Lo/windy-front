import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ContractsService } from '../service/contracts.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../model/user';
import { AuthenticationService } from '../service/authentication.service';
import { DashboardService } from '../service/dashboard.service';



@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Confirmation de la suppression</h4>
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="modal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Souhaitez-vous supprimer "{{data.companyName}}" concernant le risque situé à : "{{ data.cityRiskName }}!"</p>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-outline-secondary"
        (click)="modal.dismiss('cancel click')"
      >
        Annuler
      </button>
      <button
        type="button"
        class="btn btn-danger"
        (click)="passBack()"
      >
        Confirmer
      </button>
    </div>
  `,
})
export class NgbdModalContent {
  @Input() data: any;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public modal: NgbActiveModal) {}

  passBack() {
    this.passEntry.emit(this.data);
    this.modal.close();
    }

}

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.css']
})
export class ContractsComponent implements OnInit {


  contracts:any[]=[];
  displayedColumns: string[] = [
    // 'inceptionDate', 
    'companyName', 
  //   'coverageName', 
  // 'windSpeedThresholdCoverage', 
  // 'cityRiskName',  
  // // 'longitude', 'latitude', 
  //  'startDate', 
  //  'endDate', 
  // //  'thirdPartyMeteoStation',
  //  'InsuredCapitalPerTurbine', 
  //  'windTurbineNumber', 
  //  'daylyIndemnityPerWindTurbine', 
  //  'numberOfDayOfIndemnity', 
  //  'maxIndemnity', 
  //  'premiumRate', 
  //  'premium', 
  //  'LastUpdate', 
   ];
  dataSource: any;
  daylyIndemnityPerWindTurbine : number = 150;
  nbreEoliennes : any;
  windSpeedThresholdCoverage : number = 15;
  thirdPartyMeteoStation : string = "Visual Crossing";
  user!: User;
  nombreOfContracts : any;



  constructor(
    private contractsService:ContractsService,
    private _modalService: NgbModal,
    private dashboardService:DashboardService, 
    private authenticationService: AuthenticationService

    ) { }

  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    let emailAddress = this.user.email;
    let role = this.user.role;
    if (role == 'ROLE_USER') {
      this.getAllByEmailAddress(emailAddress);
    } else {
      this.getAll();
    } 
    this.dashboardService.getNumberOfContracts().subscribe((nbrOfContracts: any)=>{
      console.log('nombre de contrats',nbrOfContracts);
      this.nombreOfContracts=nbrOfContracts.data;
      
    });
  
  }
  getAll(){
    this.contractsService.getAllContracts().subscribe((contrats:any) => {
      // console.log("contracts :", JSON.stringify(contrats));
      this.contracts = contrats.data;
     this.dataSource = new MatTableDataSource(contrats);
     console.log("contracts :", contrats);
    })
  }

  getAllByEmailAddress(emailAddress: string) {
    this.contractsService
      .getAllContractsByUserEmail(emailAddress)
      .subscribe((contracts: any) => {
        console.log('contract :', contracts);
        this.contracts = contracts.data;
        this.dataSource = new MatTableDataSource(contracts);
      });
  }



  delete(contractId:string){
    console.log("contractId : ", contractId);
    this.contractsService.delete(contractId).subscribe((data: any) =>{
      console.log("data", data);
      this.getAll();
    })
  }
    
  open(contract: any) {
    const modalRef = this._modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = {"companyName": contract.companyName, 
    "cityRiskName": contract.cityRiskName, "id":contract._id};

    modalRef.componentInstance.passEntry.subscribe((receivedEntry: any) => {
      console.log(receivedEntry);
      this.delete(receivedEntry.id);
      })
  
}
}
