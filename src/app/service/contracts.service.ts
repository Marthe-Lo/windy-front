import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Contract } from '../model/contract';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'Application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class ContractsService {

  private host = environment.apiNodeUrl;

  saveContract(contract:Contract):Observable<Contract>{
    // http://localhost:3000
    console.log("contract in service ", contract);
    return this.http.post<Contract>(this.host+ "/contracts/create", contract);

  };

  constructor(private http: HttpClient) { }
   getAllContracts():Observable<any>{
    return this.http.get<any>(this.host+ '/contracts/getAll');
    // return this.http.get<any>(environment.apiNodeUrl+'/contracts/getAll');
  } 

getContractByReference(ref:string):Observable<any>{
  return this.http.get<any>(this.host+ '/contracts/getOneByRef/'+ ref);
}

updateContract(contract: Contract):Observable<any> {
  console.log("id? => ", contract);
  
  return this.http.put<any>(this.host+ '/contracts/updateOne', contract);
}

delete(contractId: String):Observable<any> {
  return this.http.delete<any>(this.host+ '/contracts/deleteById/' + contractId);
}

getContractDessThanSpeedAndDates(speed?:number, startDate?:Date, endDate?: Date){
  return this.http.get<any>(this.host+ '/contracts/listEtNbreJoursByContractAndSeuil/?speed='
  + speed + "&startDate"+ startDate + "&endDate"+ endDate)
}

getNumberOfEoliennes(): Observable<number> {
  return this.http.get<number>(`${this.host}/contracts/nbreEoliennes`);
}

getAllContractsByUserEmail(emailAddress: string):Observable<any>{    
  return this.http.get<any>(this.host+ '/contracts/getAllContractsByUserEmail/' + emailAddress)
}

}

