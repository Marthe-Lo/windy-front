import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Devis } from '../model/devis';
import { environment } from 'src/environments/environment';
import { CustomHttpResponse } from '../model/custom-http-response';


@Injectable({
  providedIn: 'root'
})
export class DevisService {
  private host = environment.apiNodeUrl;

  constructor(private http:HttpClient) { }

  saveDevis(devis:Devis):Observable<Devis>{
    // http://localhost:3000
    console.log("devis in service ", devis);
    return this.http.post<Devis>(this.host+ '/devis/create', devis);

  };

  getAllDevis():Observable<any>{
    return this.http.get<any>(this.host+ '/devis/getAll');
    // return this.http.get<any>(environment.apiNodeUrl+'/devis/getAll');
  } 

  getDevisById(devisId: string):Observable<any>{    
    return this.http.get<any>(this.host+ '/devis/getOne/' + devisId)
  }

  updateDevis(devis: any):Observable<any> {
    return this.http.put<any>(this.host+ '/devis/updateOne', devis)
  }

  delete(devisId: string):Observable<any> {
    return this.http.delete<any>(this.host+ '/devis/deleteById/' + devisId)
  }

  getNumberOfDevis(): Observable<number> {
    return this.http.get<number>(`${this.host}/devis/nbreDevis`);
  }

  getDevisByEmailAddress(emailAddress: string):Observable<any>{    
    return this.http.get<any>(this.host+ '/devis/getAllDevisByUserEmail/' + emailAddress)
  }
  
}
