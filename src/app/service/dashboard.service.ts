import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { Observable } from 'rxjs';
import { CustomHttpResponse } from '../model/custom-http-response';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'Application/json' }),
};
@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  private host = environment.apiNodeUrl;

  constructor(private http: HttpClient) {}

  getNumberOfContracts(): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/contracts/nbreContracts`);
  }
  

  getNumberOfCompensableDays(): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/contracts/nbreJoursByContractAndSeuil`);
  }

  // getNumberOfCompensableDays(): Observable<CustomHttpResponse> {
  //   return this.http.get<CustomHttpResponse>(`${this.host}/contracts/nbreJoursEtAndSeuilByContratindemnisable`);
  // }

  getNumberOfWindTurbine(): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/contracts/nbreEoliennes`);
  }

  getNumberOfClients(): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/contracts/nbrecompanyName`);
  }

  getNumberOfCityRisks(city: string): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/contracts/contractByCity/${city}`);
  }

 

}
