import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationType } from '../enum/notification-type.enum';
import { Role } from '../enum/role.enum';
import { FileUploadStatus } from '../model/file-upload.status';
import { User } from '../model/user';
import { AuthenticationService } from '../service/authentication.service';
import { NotificationService } from '../service/notification.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  public user!: User;
  public refreshing!: boolean;
  public fileStatus = new FileUploadStatus();
  private currentUsername!: string;
  public profileImage!: File;
  public fileName!: string;
  private subscriptions: Subscription[] = [];



  constructor(private authenticationService: AuthenticationService,private userService: UserService,private notificationService: NotificationService) { }

  ngOnInit(): void {
    
    this.user=this.authenticationService.getUserFromLocalCache();
    
  }

  
  public get isAdmin(): boolean {
    return this.getUserRole() === Role.ADMIN || this.getUserRole() === Role.SUPER_ADMIN;
  }

  public get isEmployee(): boolean {
    return this.isAdmin || this.getUserRole() === Role.EMPLOYEE;
  }

  public get isAdminOrEmployee(): boolean {
    return this.isAdmin || this.isEmployee;
  }

  private getUserRole(): string {
    return this.authenticationService.getUserFromLocalCache().role;
  }

  
  public onUpdateCurrentUser(user: User): void {
    this.refreshing = true;
    this.currentUsername = this.authenticationService.getUserFromLocalCache().username;
    const formData = this.userService.createUserFormDate(this.currentUsername, user, this.profileImage);
    this.subscriptions.push(
      this.userService.updateUser(formData).subscribe(
        (response: User) => {
          this.authenticationService.addUserToLocalCache(response);
          this.fileName = null!;
          this.profileImage = null!;
          this.notificationService.notify(NotificationType.SUCCESS, `${response.firstName} ${response.lastName} updated successfully`);
          this.refreshing = false;

        },
        (errorResponse: HttpErrorResponse) => {
          this.notificationService.notify(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
          this.profileImage = null!;
        }
      )
      );
  }


}
