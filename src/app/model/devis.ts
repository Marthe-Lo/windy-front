export class Devis {
  // public devisId: string;
  public _id!: string;
  public devisReference: string;
   public firstName: string;
   public lastName: string;
   public emailAddress: string;
   public phoneNumber: number;
   public companyName: string;
   public companyAddress: string;
   public cityRiskName: string;
   public windTurbineNumber: number;
   public startDate: Date;
   public endDate: Date;


   constructor() {
    // this.devisId = '';
    // this._id = '';
    this.devisReference = '';
     this.firstName = '';
     this.lastName = '';
     this.emailAddress = '';
     this.phoneNumber = 0;
     this.companyName = '';
     this.companyAddress = '';
     this.cityRiskName = '';
     this.windTurbineNumber = 0;
     this.startDate  = new Date();
     this.endDate  = new Date();

   }
 
  
  }
  