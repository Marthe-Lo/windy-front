export class Contract {
  public contractId: string;
  public contractReference : string;
    public inceptionDate : string;
    public companyName: string;
    public emailAddress: string;
    public coverageName : string;
    public windSpeedThresholdCoverage : number;
    public cityRiskName : string;
    public thirdPartyMeteoStation : string;
    public windTurbineNumber: number;
    public daylyIndemnityPerWindTurbine: number;
    // public maxIndemnity : number;
    // public numberOfDayOfIndemnity : number;
    // public premiumRate : number;
    // public premium : number;
    public startDate: Date;
    public endDate: Date;


    constructor() {
      this.contractId = '';
      this.contractReference = '';
      this.inceptionDate  = '';
      this.companyName = '';
      this.emailAddress = '';
      this.coverageName  = '';
      this.windSpeedThresholdCoverage  = 0;
      this.cityRiskName  = '';
      this.thirdPartyMeteoStation  = '';
      this.windTurbineNumber = 0;
      this.daylyIndemnityPerWindTurbine  = 0;
      // this.maxIndemnity  = 0;
      // this.numberOfDayOfIndemnity  = 0;
      // this.premiumRate  = 0;
      // this.premium   = 0;
      this.startDate = new Date();
      this.endDate = new Date();


    }
  
  }
  