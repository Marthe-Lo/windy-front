import { Component, OnInit } from '@angular/core';
import { ContractsService } from '../service/contracts.service';
import { DashboardService } from '../service/dashboard.service';
import { DevisService } from '../service/devis.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  nombreOfContracts : any;
  numberOfCompensableDays : any;
  nombreOfDevis : any;
  daylyIndemnityPerWindTurbine : number = 150;
  nbreEoliennes : any;


  constructor(private dashboardService:DashboardService, private devisService: DevisService, private contractsService:ContractsService) { }

  ngOnInit(): void {
    this.dashboardService.getNumberOfContracts().subscribe((nbrOfContracts: any)=>{
      console.log(nbrOfContracts);
      this.nombreOfContracts=nbrOfContracts.data;
      
    })

    this.dashboardService.getNumberOfContracts().subscribe((nbrOfContracts: any)=>{
      console.log('nombre de contrats',nbrOfContracts);
      this.nombreOfContracts=nbrOfContracts.data;
      
    });

    this.dashboardService.getNumberOfCompensableDays().subscribe((numberOfCompensableDays: any)=>{
      console.log('jours compensable',numberOfCompensableDays);
      this.numberOfCompensableDays=numberOfCompensableDays.data;      
    });

  
    this.devisService.getNumberOfDevis().subscribe((nbreOfDevis:any)=>{
      console.log("nombre of devis :", nbreOfDevis);
      this.nombreOfDevis=nbreOfDevis.data;      
    })

    this.contractsService.getNumberOfEoliennes().subscribe((nbEoliennes:any)=>{
      console.log("nombre de devis :", nbEoliennes);
      this.nbreEoliennes=nbEoliennes.data;
      
    })

  }
}
