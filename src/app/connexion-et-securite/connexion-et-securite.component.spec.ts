import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionEtSecuriteComponent } from './connexion-et-securite.component';

describe('ConnexionEtSecuriteComponent', () => {
  let component: ConnexionEtSecuriteComponent;
  let fixture: ComponentFixture<ConnexionEtSecuriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnexionEtSecuriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnexionEtSecuriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
