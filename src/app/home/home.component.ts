import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { PrimeNGConfig } from 'primeng/api';
import { Subscription } from 'rxjs';
import { HeaderType } from 'src/app/enum/header-type.enum';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { NotificationService } from 'src/app/service/notification.service';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public showLoading!: boolean;
  private subscriptions: Subscription[] = [];
  public user: any;
  public showlogin : boolean = true;
  

  constructor (private router: Router, private authenticationService:AuthenticationService, private userService:UserService,
    private notificationService: NotificationService, private primengConfig: PrimeNGConfig) { }
  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    console.log(this.user);
    if(this.user != null){
      this.showlogin=false;
    }

    
  }  
  public onLogin(user: User): void {
    this.showLoading = true;
    this.subscriptions.push(
      this.authenticationService.login(user).subscribe(
        (response: HttpResponse<User> | any) => {
          const token = response.headers.get(HeaderType.JWT_TOKEN);
          this.authenticationService.saveToken(token);
          this.authenticationService.addUserToLocalCache(response.body);
          this.router.navigateByUrl('/admin/dashboard');
          this.showLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this.notificationService.notify(NotificationType.ERROR, errorResponse.error.message);
          this.showLoading = false;
        }
      )
    );
  }

  public redirect(){
    this.router.navigateByUrl("/admin/dashboard");
    console.log("redirection ");
    
  }

}
