import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute, UrlSegment, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { NotificationService } from '../service/notification.service';
import { NotificationType } from '../enum/notification-type.enum';
import { filter } from 'rxjs/operators';
import { PartialObserver } from 'rxjs';
import { DashboardrouteService } from '../service/dashboardroute.service';

@Injectable({providedIn: 'root'})
export class AuthenticationGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService, private activatedRoute: ActivatedRoute, private router: Router,
              private notificationService: NotificationService, private dashboardRouteService : DashboardrouteService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.isUserLoggedIn();
  }

  private isUserLoggedIn(): boolean {
    // console.log("test accueil ", this.router.url);
    let url = this.router.url;

    // console.log(this.dashboardRouteService.getCurrentUrl());
    // console.log(this.dashboardRouteService.getPreviousUrl());    
    

    if (this.authenticationService.isUserLoggedIn() && url != "/home") {
      console.log("test login ");
      return true;      
    }else{ 
    this.router.navigate(['/login']);
    this.notificationService.notify(NotificationType.ERROR, `You need to log in to access this page`);
    return false;
  }
  }

}
