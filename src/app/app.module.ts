import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationService } from './service/authentication.service';
import { UserService } from './service/user.service';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { AuthenticationGuard } from './guard/authentication.guard';
import { NotificationModule } from './notification.module';
import { NotificationService } from './service/notification.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';

import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import { ContractsComponent } from './contracts/contracts.component';
import { ProfilComponent } from './profil/profil.component';
import { DevisComponent } from './devis/devis.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MenuModule } from 'primeng/menu';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { MesDevisComponent } from './mes-devis/mes-devis.component';
import { MesContratsComponent } from './mes-contrats/mes-contrats.component';
import { MonProfilComponent } from './mon-profil/mon-profil.component';
import { ConnexionEtSecuriteComponent } from './connexion-et-securite/connexion-et-securite.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminComponent } from './admin/admin.component';
import { NewDevisComponent } from './devis/new-devis/new-devis.component';
import { NewUserComponent } from './user/new-user/new-user.component';
import { NewContractComponent } from './contracts/new-contract/new-contract.component';
import { HomeComponent } from './home/home.component';
import { ClientComponent } from './client/client.component';
import {DpDatePickerModule} from 'ng2-date-picker';


// import { MatMenuModule } from '@material/menu';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    ContractsComponent,
    ProfilComponent,
    DevisComponent,
    MesDevisComponent,
    MesContratsComponent,
    MonProfilComponent,
    ConnexionEtSecuriteComponent,
    NewDevisComponent,
     NewContractComponent,
    NewUserComponent,
    AdminComponent,
    FooterComponent,
    HomeComponent, 
    ClientComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    NotificationModule,
    BrowserAnimationsModule,
    MatSidenavModule,     
    MatFormFieldModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    FlexLayoutModule,
    // MatMenuModule,
    MenuModule,
    RippleModule,
    ButtonModule,
    ReactiveFormsModule,
    InputTextareaModule,
    NgbModule,
    DpDatePickerModule
  ],
  providers: [NotificationService, AuthenticationGuard, AuthenticationService, UserService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
