export enum Role {
    SUPER_ADMIN = 'ROLE_SUPER_ADMIN',
    ADMIN = 'ROLE_ADMIN',
    EMPLOYEE = 'ROLE_EMPLOYEE',
    USER = 'ROLE_USER',
    HR = 'ROLE_HR'
  }
  