import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Devis } from 'src/app/model/devis';
import { DevisService } from 'src/app/service/devis.service';

@Component({
  selector: 'app-new-devis',
  templateUrl: './new-devis.component.html',
  styleUrls: ['./new-devis.component.css'],
})
export class NewDevisComponent implements OnInit {
  devisForm!: FormGroup;
  devisId?: string | null;
  devis!: Devis;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private newDevisService: DevisService
  ) {}

  ngOnInit(): void {
    this.devisForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      emailAddress: new FormControl(''),
      phoneNumber: new FormControl(''),
      companyName: new FormControl(''),
      companyAddress: new FormControl(''),
      cityRiskName: new FormControl(''),
      windTurbineNumber: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
    });

    this.devisId = this.route.snapshot.paramMap.get('id');
    console.log('devisId ', this.devisId);

    if (this.devisId !== null) {
      this.newDevisService.getDevisById(this.devisId).subscribe((data: any) => {
        this.devis = data.data;
        console.log('devis :', this.devis);

        this.devisForm.patchValue({
          firstName: data.data.firstName,
          lastName: data.data.lastName,
          emailAddress: data.data.emailAddress,
          phoneNumber: data.data.phoneNumber,
          companyName: data.data.companyAddress,
          companyAddress: data.data.companyAddress,
          cityRiskName: data.data.cityRiskName,
          windTurbineNumber: data.data.windTurbineNumber,
          startDate: data.data.startDate,
          endDate: data.data.endDate,
        });
        console.log('data : ', data.data);
      });
    }
  }

  formValidation() {
    this.devis= new Devis();    
    this.devis.firstName = this.devisForm.value.firstName;
    this.devis.lastName = this.devisForm.value.lastName;
    this.devis.emailAddress = this.devisForm.value.emailAddress;
    this.devis.phoneNumber = this.devisForm.value.phoneNumber;
    this.devis.companyName = this.devisForm.value.companyName;
    this.devis.companyAddress = this.devisForm.value.companyAddress;
    this.devis.cityRiskName = this.devisForm.value.cityRiskName;
    this.devis.windTurbineNumber = this.devisForm.value.windTurbineNumber;
    this.devis.startDate = this.devisForm.value.startDate;
    this.devis.endDate = this.devisForm.value.endDate;
console.log("devis = ", this.devis);

    if (this.devis._id) {
      this.newDevisService.updateDevis(this.devis).subscribe((data) => {
        console.log('data : ', data);
      });
    } else {
      this.newDevisService.saveDevis(this.devis).subscribe((data: any) => {
        console.log(data);
        setTimeout(() => {
          this.router.navigate(['./admin/devis']);
        }, 1000);
      });
    }
  }
  delete(devisId : string){
    console.log("devisId :" , devisId);
    this.newDevisService.delete(devisId).subscribe((data:any)=>{
      console.log("data ", data);
      
    })
  }


  demo() {
    this.devisForm.setValue({
      firstName: 'Jean',
      lastName: 'VALJEAN',
      emailAddress: 'Jean.valjean@gmail.com',
      phoneNumber: '0606060',
      companyName: 'duchmol',
      companyAddress: 'bidule',
      cityRiskName: 'truc',
      windTurbineNumber: 2,
      startDate: new Date(),
      endDate: new Date(),
    });
  }
}
