import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DevisService } from '../service/devis.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../model/user';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Confirmation de la suppression</h4>
      <button
        type="button"
        class="close"
        aria-label="Close"
        (click)="modal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>
        Souhaitez-vous supprimer "{{ data.companyName }}" concernant le risque
        situé à : "{{ data.cityRiskName }}!"
      </p>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-outline-secondary"
        (click)="modal.dismiss('cancel click')"
      >
        Annuler
      </button>
      <button type="button" class="btn btn-danger" (click)="passBack()">
        Confirmer
      </button>
    </div>
  `,
})
export class NgbdModalContent {
  @Input() data: any;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(public modal: NgbActiveModal) {}

  passBack() {
    this.passEntry.emit(this.data);
    this.modal.close();
  }
}

@Component({
  selector: 'app-devis',
  templateUrl: './devis.component.html',
  styleUrls: ['./devis.component.css'],
})
export class DevisComponent implements OnInit {
  devis: any[] = [];
  daylyIndemnityPerWindTurbine: number = 150;
  windSpeedThresholdCoverage: number = 15;
  thirdPartyMeteoStation: string = 'Visual Crossing';

  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'emailAddress',
    'phoneNumber',
    'companyName',
    'companyAddress',
    'cityRiskName',
    'windTurbineNumber',
    'startDate',
    'endDate',
  ];
  dataSource: any;
  user!: User;
  nombreOfDevis : any;


  constructor(
    private devisService: DevisService,
    private _modalService: NgbModal,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    let emailAddress = this.user.email;
    let role = this.user.role;
    if (role == 'ROLE_USER') {
      this.getAllByEmailAddress(emailAddress);
    } else {
      this.getAll();
    }

    
    this.devisService.getNumberOfDevis().subscribe((nbreOfDevis:any)=>{
      console.log("nombre of devis :", nbreOfDevis);
      this.nombreOfDevis=nbreOfDevis.data;      
    })
  }

  getAll() {
    this.devisService.getAllDevis().subscribe((deviss: any) => {
      // console.log("devis :", JSON.stringify(deviss));
      console.log('devis :', deviss);
      this.devis = deviss.data;
      this.dataSource = new MatTableDataSource(deviss);
    });
  }

  getAllByEmailAddress(emailAddress: string) {
    this.devisService
      .getDevisByEmailAddress(emailAddress)
      .subscribe((deviss: any) => {
        // console.log("devis :", JSON.stringify(deviss));
        console.log('devis :', deviss);
        this.devis = deviss.data;
        this.dataSource = new MatTableDataSource(deviss);
      });
  }

  delete(devisId: string) {
    console.log('devisId : ', devisId);
    this.devisService.delete(devisId).subscribe((data: any) => {
      console.log('data', data);
      this.getAll();
    });
  }

  open(devis: any) {
    const modalRef = this._modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = {
      companyName: devis.companyName,
      cityRiskName: devis.cityRiskName,
      id: devis._id,
    };

    modalRef.componentInstance.passEntry.subscribe((receivedEntry: any) => {
      console.log(receivedEntry);
      this.delete(receivedEntry.id);
    });
  }
}
