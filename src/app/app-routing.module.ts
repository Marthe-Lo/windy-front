import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { AuthenticationGuard } from './guard/authentication.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ProfilComponent } from './profil/profil.component';
import { DevisComponent } from './devis/devis.component';
import { MesDevisComponent } from './mes-devis/mes-devis.component';
import { MesContratsComponent } from './mes-contrats/mes-contrats.component';
import { MonProfilComponent } from './mon-profil/mon-profil.component';
import { ConnexionEtSecuriteComponent } from './connexion-et-securite/connexion-et-securite.component';
import { NewContractComponent } from './contracts/new-contract/new-contract.component';
import { NewDevisComponent } from './devis/new-devis/new-devis.component';
import { NewUserComponent } from './user/new-user/new-user.component';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './home/home.component';
import { ClientComponent } from './client/client.component';


const routes: Routes = [

  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent },

  { path: 'admin', component: AdminComponent, children:[        
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthenticationGuard] },
    { path: 'client', component: ClientComponent, canActivate: [AuthenticationGuard] },
    { path: 'profil', component: ProfilComponent, canActivate: [AuthenticationGuard] },
      
      { path: 'devis', component: DevisComponent, canActivate: [AuthenticationGuard] },
      { path: 'devis/newdevis', component: NewDevisComponent, canActivate: [AuthenticationGuard] },
      { path: 'devis/devis/:id', component: NewDevisComponent, canActivate: [AuthenticationGuard] },
      
      { path: 'contracts', component: ContractsComponent, canActivate: [AuthenticationGuard] },
      { path: 'contracts/newcontract', component: NewContractComponent, canActivate: [AuthenticationGuard] },
      { path: 'contracts/contract/:id', component: NewContractComponent, canActivate: [AuthenticationGuard] },

      { path: 'users', component: UserComponent, canActivate: [AuthenticationGuard] },
      { path: 'users/newuser', component: NewUserComponent, canActivate: [AuthenticationGuard] },
      
      { path: 'mesrequetes', component: MesDevisComponent, canActivate: [AuthenticationGuard] },
      { path: 'mescontrats', component: MesContratsComponent, canActivate: [AuthenticationGuard] },
      { path: 'monprofil', component: MonProfilComponent, canActivate: [AuthenticationGuard] },
      { path: 'connexionetsecurite', component: ConnexionEtSecuriteComponent, canActivate: [AuthenticationGuard] },
  ]},

  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
