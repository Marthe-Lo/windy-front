import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { User } from 'src/app/model/user';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'src/app/service/notification.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Role } from 'src/app/enum/role.enum';
import { Router } from '@angular/router';



@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  public profileImage!: File;
  private subscriptions: Subscription[] = [];
  public fileName!: string;
  public refreshing!: boolean;
  public users!: User[];
  public user!: User;
  public verifAdmin : boolean = false;
  public verifAdminOrEmployee : boolean = false;



  constructor(private userService: UserService, private router: Router, 
    private authenticationService: AuthenticationService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    this.getUsers(true);
    if(this.user.role === Role.ADMIN || this.user.role === Role.SUPER_ADMIN){
      this.verifAdmin = true;
    }
    else if(this.user.role === Role.EMPLOYEE){
      this.verifAdminOrEmployee = true;
    }
  }

  
  public onAddNewUser(userForm: NgForm): void {
    const formData = this.userService.createUserFormDate(null!, userForm.value, this.profileImage);
    this.subscriptions.push(
      this.userService.addUser(formData).subscribe(
        (response: User) => {          
          this.clickButton('new-user-close');
          this.getUsers(false);
          this.fileName = null!;
          this.profileImage = null!;
          userForm.reset();
          this.notificationService.notify(NotificationType.SUCCESS, `${response.firstName} ${response.lastName} added successfully`);
          setTimeout(()=>{this.router.navigate(['users']);
        },
          1000
          );

        },
        (errorResponse: HttpErrorResponse) => {
          this.notificationService.notify(NotificationType.ERROR, errorResponse.error.message);
          this.profileImage = null!;
        }
      )
      );
  }
  
  private clickButton(buttonId: string): void {
    document.getElementById(buttonId)?.click();
  }

  public getUsers(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.userService.getUsers().subscribe(
        (response: User[]) => {
          this.userService.addUsersToLocalCache(response);          
          this.users = response;

          this.refreshing = false;
          if (showNotification) {
            // this.sendNotification(NotificationType.SUCCESS, `${response.length} user(s) loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.notificationService.notify(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }
  
  public saveNewUser(): void {
    this.clickButton('new-user-save');
  }


}
